**Selenium 自动化测试程序（Python版）**
运行环境：
- selenium web driver
- python3
- pytest
- git

**驱动版本下载：**

ChromeDriver：https://npm.taobao.org/mirrors/chromedriver/

FirefoxDriver：https://npm.taobao.org/mirrors/geckodriver/

OperaDriver：https://github.com/operasoftware/operachromiumdriver/releases

MicrosoftEdgeDriver：https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/#downloads

运行命令：
pytest -sv cases/test_baidu.py --alluredir ./allure_results